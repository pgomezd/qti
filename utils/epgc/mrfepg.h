//#define DEBUG 

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <math.h>
#include <complex.h> 
#include <errno.h>


static const char adiabatic[] = "adiabatic";
typedef enum { false, true } bool;


typedef struct mrfmetha_s{

	double tr;
	double excPulse;
	double invPulseDuration;
	char *invPulseEnd;
	bool fispInvEnabled;
	double phaseAdvance;
	double te;
	double ti;
	double *variableFA;
	double *variableTR;
	int numFAs;
	double invOffset;
	double freq;
	double b1Map;

} mrfmetha;


int mrfepg(long double complex *out, double t1, double t2, mrfmetha *method);
