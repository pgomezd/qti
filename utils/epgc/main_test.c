
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "mrfepgbv.h"

#define TRFL "../../data/tr.txt"
#define FAFL "../../data/fa.txt"
#define B1FL "../../data/B1_slice.txt"
#define MAXFAS 260
#define MAXB1 14


int readfile(double values[], char *filename){
        int linesize = 50;
        char buffer[linesize];
        int nfas = 0;

        FILE *f = fopen(filename, "r");

        if (!f){return -1;}

        while(fgets(buffer,linesize,f) != NULL){
                values[nfas] = strtod(buffer, NULL);
                nfas = nfas + 1;
        }

        fclose(f);

        return nfas;
}

double get_min(double *a, int la){
        int i;
        double min = DBL_MAX;

        for ( i = 0; i < la; i++ )
                if ( a[i] < min )
                        min = a[i];

        return min;
}


int main(int argc, char *argv[])
{

	// Read FA TR and B1
	double *fa = calloc(MAXFAS, sizeof(double));
	double *tr = calloc(MAXFAS, sizeof(double));
	double *b1 = calloc(MAXB1,  sizeof(double));

	int nfa = readfile(fa,FAFL);
	int ntr = readfile(tr,TRFL);
	int nb1 = readfile(b1,B1FL);

	if (nfa != ntr){
	  fprintf(stderr,"** The number of TRs(%d) is different from FAs(%d)\n",ntr,nfa);
	  abort();
	}



	int numsamples = MAXFAS;

	mrfmetha method;

	double sig[nfa];
	long double complex sig1[nfa];
	float t1 = 1;
	float t2 = 0.3;
	float v = 50;
	int i;

	method.numFAs = nfa;
	method.variableFA = fa;
	method.variableTR = tr;
	method.b1Slice = b1;
	method.numB1Slice = nb1;
	method.L = 0.2;


	// Run model    

	method.excPulse = 0.002;
	method.invPulseDuration = 0.010;
	method.invPulseEnd = "adiabatic";
	method.ti = 0.018;
	method.fispInvEnabled = true;
	method.phaseAdvance = 180;
	method.te = 0.002;
	method.invOffset = 1;
	method.freq = 0;
	method.b1Map = 1;
	method.tr = get_min(tr,nfa);


	mrfepg(sig1, t1, t2, v, &method);

	// Write to file
	FILE *fp = fopen("mrfepgb1.txt", "w+");

	for ( i = 0; i < method.numFAs; i++){
	        sig[i] = cabsl(sig1[i]);
	        fprintf(fp, "%.5f\n",sig[i]);
	}

	fclose(fp);

}