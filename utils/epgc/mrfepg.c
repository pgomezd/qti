/*
 * mrfepg.c
 * 
 * Magnetice resonance fingerprinting simulator.
 *
 * Code translated from MATLAB to C by Miguel Molina at TUM based on 
 * the original MATLAB code from Guido Buonincontri (IMAGO7).
 *
 */

#include "mrfepg.h"
//#define DEBUG 1


 extern int errno;

#ifdef DEBUG
 void printfarray(long double complex **A, char* array, int rows, int cols){
 	int g,h;
 	for ( g = 0; g < rows; ++g)
 	{
 		for ( h = 0; h < cols; h++){
 			printf("## %s[%d][%d] = %Le + i%Le \n",array,g,h,creall(A[g][h]),cimagl(A[g][h]));
 		}
 	}
 }
#endif

 void RFpulse(long double complex **T, double fa, double phi, double db0, double trf){

#ifdef DEBUG
 	printf("\n###### RFpulse ####### \n");
#endif

 	long double b1 = fa/trf;
 	long double beff = sqrt( db0*db0 + b1*b1 );
 	long double c = cos(beff * trf);
 	long double s = sin(beff * trf);
 	long double b1beff = b1/beff;
 	long double db0beff = db0/beff;


#ifdef DEBUG
 	printf("## b1=%Lf \n",b1);
 	printf("## beff=%Lf \n",beff);
 	printf("## c=%Lf \n",c);
 	printf("## s=%Lf \n",s);
 	printf("## b1beff=%Lf \n",b1beff);
 	printf("## db0beff=%Lf \n\n",db0beff);
#endif

 	T[0][0] = 0.5 * (b1beff*b1beff + (1 + db0beff*db0beff) * c) + I * db0beff * s;
 	T[0][1] = 0.5 * b1beff*b1beff * (1 - c) * cexp(I * 2 * phi);
 	T[0][2] = b1beff * ( db0beff * (1 - c) - I * s ) * cexp(I * phi);
 	T[1][0] = conj(T[0][1]);
 	T[1][1] = conj(T[0][0]);
 	T[1][2] = conj(T[0][2]);
 	T[2][0] = 0.5 * b1beff * ( db0beff * (1 - c) - I * s ) * cexp(I * phi);
 	T[2][1] = conj(T[2][0]);
 	T[2][2] = db0beff*db0beff + b1beff*b1beff * c;

#ifdef DEBUG
 	printfarray(T,"T",3,3);
#endif

 }

 void multiply_martix(long double complex **out, long double complex **m1, int m1rows, int m1cols, long double complex **m2, int m2rows, int m2cols){

 	if ( m1cols != m2rows ) {  
 		perror("Imposible to multiply: inconsitent matrix dimensions.");
 		return;
 	}

#ifdef DEBUG
 	printf("\n###### multiply_matrix ####### \n");
 	printfarray(m1,"m1",m1rows,m1cols);
 	printfarray(m2,"m2",m2rows,m2cols);
#endif

 	long double complex sum;
 	int c,r,i;

 	for (r = 0; r < m1rows; r++){
 		for (c = 0; c < m2cols; c++){
 			sum = 0;
 			for (i = 0; i < m1cols; i++){
 				sum = sum + m1[r][i] * m2[i][c];
 			}
 			out[r][c] = sum;
		#ifdef DEBUG
 			printf("## out[%d][%d] = %Le +i%Le  \n",r,c,creall(out[r][c]),cimagl(out[r][c]));
		#endif
 		}
 	}

 }


 void multiply_array_scalar(long double complex *out, long double complex *array, int arrlen, double scalar){

#ifdef DEBUG
 	printf("\n###### multiply_array_scalar ####### \n");
#endif

 	int i;
 	for (i = 0; i<arrlen; i++){
 		out[i] = array[i] * scalar;
	#ifdef DEBUG
 		printf("## out[%d] = %Le +i%Le \n",i,creall(out[i]), cimagl(out[i]));
	#endif
 	}
 }


 void copy_matrix(long double complex **dst,long double complex **src, int rows, int cols){
 	int r,c;
 	for ( r = 0; r < rows; r++){
 		for ( c = 0; c < cols; c++){
 			dst[r][c] = src[r][c];
 		}
 	}
 }


/*
 * Notice that in this implementation t1 and t2 are scalar values.
 * No bSSFP support
 */
 int mrfepg(long double complex *sig, double t1, double t2, mrfmetha *method){


#ifdef DEBUG
 	printf("\n###### MRFEPG ####### \n");
#endif

/************** GET PARAMS FROM SEQUENCE **************/
 	int Nold = 20; /*20180321. pgd - 8 -> 20*/
 	int nfas = method->numFAs;
 	double trmin = 0;
 	double trf = method->excPulse;
 	double trfinv = method->invPulseDuration;
 	double tev = method->ti;
 	double b1Map =  method->b1Map;
 	double phadv = method->phaseAdvance;
 	double freq = method->freq * 2 * M_PI;
 	double te = method->te;
 	double an;
 	long double e1,e2,ete1,ete2,fan;
 	double phi,tr,tpad;

 	double fa[nfas];
 	memcpy(fa,method->variableFA,nfas*sizeof(double));

 	double delay[nfas];
 	memcpy(delay,method->variableTR,nfas*sizeof(double));

 	long double complex phoff, phoffte;

 	long double complex **F = malloc(3*sizeof(double*)); 
 	long double complex **Faux = malloc(3*sizeof(double*)); 
 	long double complex **T = malloc(3*sizeof(double*)); 

 	int c;
 	for (c = 0; c < 3; c++){
 		F[c] = calloc(Nold,sizeof(phoff));
 		Faux[c] = calloc(Nold,sizeof(phoff));
 		T[c] = calloc(3,sizeof(phoff));	
 	}

#ifdef DEBUG
 	printf("## Complex size: %lu\n",sizeof(phoff));
 	printf("## Number of flip angles: %d\n",nfas);
 	printf("## FA[0]=%f, FA[end]=%f \n",fa[0],fa[nfas-1]);
 	printf("## TR[0]=%f, TR[end]=%f \n",delay[0],delay[nfas-1]);
 	printf("## TE=%f \n", te);
 	printf("## T1=%f \n",t1);
 	printf("## T2=%f \n",t2);
 	printf("## phadv=%f \n",phadv);	
 	int g,h;
#endif

/************** START PULSE SEQUENCE **************/

F[2][0] = 1;       // Equilibrium Magnetization


if ( method->fispInvEnabled ) {

	#ifdef DEBUG
	printf("## FISP Inversion enabled\n");
	#endif

	if ( strcmp(method->invPulseEnd,adiabatic) == 0) {
		an = M_PI * method->invOffset;
		RFpulse(T,an,0,0,trf);

		#ifdef DEBUG
		printf("## FISP Adiabatic end wiht angle: %f\n", an);
		#endif
	}
	else {
		an = M_PI * b1Map;
		RFpulse(T,an,0,freq,trf);
	}

	multiply_martix(Faux, T, 3,3, F, 3, Nold-1);
	copy_matrix(F,Faux,3,Nold-1);


	/************** RELAXATION **************/
	e1 = exp(-1 * tev/t1);
	e2 = exp(-1 * tev/t2);
	phoff = cexp( I * freq * (tev-trfinv)/2);

	#ifdef DEBUG
	printf("## e1: %Lf \n", e1);
	printf("## e2: %Lf \n", e2);	
	printf("## phoff: %Lf + i%Lf\n", creall(phoff), cimagl(phoff));
	#endif

	multiply_array_scalar(F[2], F[2], Nold, e1); // T1 relaxation for Z with k > 0

	F[2][0] = F[2][0] + 1 - e1; // T1 recovery for Z with k = 0

	/***************************************/

}

phi = 0;

ete1 = exp(-1 * te/t1);
ete2 = exp(-1 * te/t2);
phoffte = cexp(I * freq * (te - trf/2));

#ifdef DEBUG
printf("## ete1: %Lf \n", ete1);
printf("## ete2: %Lf \n", ete2);	
printf("## phoffte: %Lf + i%Lf\n", creall(phoffte), cimagl(phoffte));
#endif


int n,nn;
for (n = 0; n<nfas; n++){

// Phase cycling
	phi = phi + phadv/180 * M_PI;
	fan = b1Map * fa[n] * M_PI / 180;
	tr = trmin + delay[n];

// Pulse
	RFpulse(T,fan,phi,freq,trf);
	multiply_martix(Faux, T, 3,3, F, 3, Nold-1);
	copy_matrix(F,Faux,3,Nold-1);

// Relaxation
	tpad = tr-te;
	e1 = exp(-tpad/t1);
	e2 = exp(-tpad/t2);
	phoff = cexp(I * freq * (tpad-trf/2));

	#ifdef DEBUG
	printf("## fa: %Le \n", fan*180/M_PI);
	printf("## tpad: %f \n", tpad);
	printf("## e1: %Lf \n", e1);
	printf("## e2: %Lf \n", e2);	
	printf("## phoff: %Lf + i%Lf\n", creall(phoff), cimagl(phoff));
	#endif

// Relaxation
	multiply_array_scalar(F[2], F[2], Nold, ete1); // T1 relaxation for Z with k > 0
	F[2][0] = F[2][0] + 1 - ete1; // T1 recovery for Z with k = 0

	#ifdef DEBUG
	printfarray(F,"F",3,Nold);
	#endif

	multiply_array_scalar(F[0], F[0], Nold, ete2); // T2 relaxation for F+ and F-     
	multiply_array_scalar(F[1], F[1], Nold, ete2); 
	multiply_array_scalar(F[0], F[0], Nold, phoffte);
	multiply_array_scalar(F[1], F[1], Nold, conj(phoffte)); 

	#ifdef DEBUG
	printfarray(F
		,"F",3,Nold);
	#endif


// Acquire
	sig[n] = conj( cexp(-1 * I * phi) ) * F[0][0];

	#ifdef DEBUG
	printf("\n## Signal intensity it%d: %Le + %Lei\n\n", n,creall(sig[n]),cimagl(sig[n]));
	#endif

// Relaxation
	multiply_array_scalar(F[2], F[2], Nold, e1); // T1 relaxation for Z with k > 0
	F[2][0] = F[2][0] + 1 - e1; // T1 recovery for Z with k = 0
	multiply_array_scalar(F[0], F[0], Nold, e2); // T2 relaxation for F+ and F-     
	multiply_array_scalar(F[1], F[1], Nold, e2); 
	multiply_array_scalar(F[0], F[0], Nold, phoff);
	multiply_array_scalar(F[1], F[1], Nold, conj(phoff)); 

	copy_matrix(Faux,F,3,Nold);
	for (nn = 1; nn < Nold; nn++){ F[0][nn] = Faux[0][nn-1]; } // dephase F+
	for (nn = 0; nn < Nold-1; nn++){ F[1][nn] = Faux[1][nn+1]; } // dephase F+
		F[1][Nold-1] = 0;
	F[0][0] = conj(F[1][0]); //generate conjugate pendant F+0 from F-0 

	#ifdef DEBUG
	printfarray(F
		,"F",3,Nold);
	#endif

}


return 0;
}

